import pandas as pd
from src.utils.lead_partner_identifier import lead_partner_identifier, PartnerType
from src.utils.lead_state_identifier import lead_state_identifier, LeadStates
from src.create_branch_df import create_branches_df
from src.utils.utils import query_db


def get_branch_code(branch_lead_code):
    # This function queries the branch DB and then gets the
    try:
        branch_doc = query_db("vymo-lms", "indiafirstlife_branches", {"code": branch_lead_code})
        return branch_doc[0]["inputs_map"]["branch_code"]
    except KeyError:
        return float("NaN")


def process_updates(record, df_row):
    updates_types = []
    updates_times = []
    updates_planned_times = []
    for i, update in enumerate(record["updates"]):
        try:
            planned_time = update["planned_date"].timestamp()
        except KeyError:
            planned_time = None
        updates_times.append(update["date"].timestamp())
        updates_types.append(update["type"])
        updates_planned_times.append(planned_time)

    df_row["update_types"] = str(updates_types)
    df_row["update_timestamps"] = str(updates_times)
    df_row["update_planned_timestamps"] = str(updates_times)
    return df_row


def process_mongo_leads(documents, data_frame, branches):

    for doc in documents:
        df_new_row = {}
        try:
            df_new_row["db_id"] = doc["_id"]
        except KeyError:
            pass

        try:
            df_new_row["user_id"] = doc["user"]["code"]
        except KeyError:
            pass

        try:
            if doc["assigned_by"]["code"] == doc["user"]["code"]:
                df_new_row["self_sourced"] = 100
            else:
                df_new_row["self_sourced"] = 0
        except KeyError:
            pass

        try:
            df_new_row["dist_travelled"] = doc["inputs_map"]["travel_distance"]
        except KeyError:
            pass

        try:
            df_new_row["ticket_size"] = doc["inputs_map"]["ticket_size"]
        except KeyError:
            pass

        try:
            df_new_row["premium"] = doc["inputs_map"]["collection_premium"]
            # This is present for persistent leads.
        except KeyError:
            pass

        # This is required to identify the partner type associated with the lead.
        # try:
        #     df_new_row["lead_activity_type"] = doc["inputs_map"]["_activity_code"]
        # except KeyError:
        #     pass

        # This is required to identify the tenure of the user. (first-lead-date to latest-lead-date)
        try:
            df_new_row["server_date"] = doc["server_date"].timestamp()
        except KeyError:
            pass

        # The below two fields are required to identify the LeadState and PartnerType.
        try:
            df_new_row["last_update_type"] = doc["last_update_type"]
        except KeyError:
            pass
        try:
            df_new_row["first_update_type"] = doc["first_update_type"]
        except KeyError:
            pass

        # Update meeting details
        try:
            df_new_row["meeting_duration"] = doc["meeting"]["duration"]
        except KeyError:
            pass
        try:
            df_new_row["meeting_date"] = doc["meeting"]["date"].timestamp()
        except KeyError:
            pass

        # Branch code information
        all_inputs = doc["inputs"]
        branch_lead_code = None
        for i in range(len(all_inputs)):
            if all_inputs[i]["code"] == "branch_name":
                try:
                    branch_lead_code = all_inputs[i]["data"]["code"]
                except KeyError:
                    pass
                break
        if branch_lead_code:
            df_new_row["branch_code"] = branches.loc[branches["branch_lead_code"] == branch_lead_code,
                                                     "branch_code"].values[0]
            df_new_row["branch_size"] = branches.loc[branches["branch_lead_code"] == branch_lead_code,
                                                     "branch_size"].values[0]

        # Set Partner Type
        partner = lead_partner_identifier(doc)
        df_new_row["partner_type"] = PartnerType(partner).name

        # Set lead state
        lead_state = lead_state_identifier(doc)
        df_new_row["lead_state"] = LeadStates(lead_state).name

        # Process activities
        df_new_row = process_updates(doc, df_new_row)

        # Set expected leads, converted leads and expected premium.
        try:
            df_new_row["expected_leads"] = doc["inputs_map"]["expected_leads"]
        except KeyError:
            pass

        try:
            df_new_row["number_leads"] = doc["updates_inputs_map"]["activity_completed"]["number_leads"]
        except KeyError:
            pass

        try:
            df_new_row["expected_premium"] = doc["updates_inputs_map"]["activity_completed"]["expected_premium"]
        except KeyError:
            pass

        data_frame = data_frame.append(df_new_row, ignore_index=True)

    return data_frame


if __name__ == '__main__':
    import time
    st = time.time()

    db = "vymo-lms"
    table = "indiafirstlife_leads"
    all_leads = query_db(db, table)
    all_per_leads = query_db(db, "indiafirstlife_per_leads")
    all_branches = query_db(db, "indiafirstlife_branches")

    # Fill in the branches data frame
    branches = create_branches_df(all_branches)

    # Fill in the leads table
    leads_df = process_mongo_leads(all_leads, pd.DataFrame(), branches)
    leads_df = process_mongo_leads(all_per_leads, leads_df, branches)
    leads_df.to_csv("/Users/shreyasrk/PycharmProjects/ifl_user_profiler/data/leads/leads_df.tsv", index=False, sep="\t")

    et = time.time()
    pass # print("Total Time", et-st)
