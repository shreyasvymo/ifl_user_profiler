from datetime import datetime
from src.utils.lead_state_transitions_ifl_mapping import *


def is_backlog(document):
    can_update = document["can_update"]
    has_meeting = document["has_meeting"]
    try:
        meeting_date = document["meeting"]["date"].timestamp()
    except KeyError:
        meeting_date = None

    if can_update and has_meeting and meeting_date:
        if meeting_date < datetime.now().timestamp():
            return True
    return False


def is_won(last_update_type):
    if last_update_type in lead_state_activity_mapping[LeadStates.won_state.value]:
        return True
    return False


def is_lost(last_update_type):
    if last_update_type in lead_state_activity_mapping[LeadStates.lost_state.value]:
        return True
    return False


def lead_state_identifier(lead_doc):
    """
    This function is used to identify the state for each lead.
    Different states of a lead could be ['won', 'lost', 'backlog', 'active']
    :param lead_doc:
    :return:
    """
    try:
        last_update_type = lead_doc["last_update_type"]
    except KeyError:
        return LeadStates.active_state.value

    if is_won(last_update_type):
        return LeadStates.won_state.value
    elif is_lost(last_update_type):
        return LeadStates.lost_state.value
    elif is_backlog(lead_doc):
        return LeadStates.backlog_state.value
    else:
        return LeadStates.active_state.value


def get_update_type(update):
    # This function returns the LeadState enums for the given updates
    if update in lead_state_activity_mapping[LeadStates.rescheduled_state.value]:
        return LeadStates.rescheduled_state.value
    elif update in lead_state_activity_mapping[LeadStates.met_state.value]:
        return LeadStates.met_state.value
    elif update in lead_state_activity_mapping[LeadStates.won_state.value]:
        return LeadStates.won_state.value
    elif update in lead_state_activity_mapping[LeadStates.lost_state.value]:
        return LeadStates.lost_state.value
    else:
        return 0
