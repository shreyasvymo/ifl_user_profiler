from enum import Enum


class PartnerType(Enum):
    cif = 1
    branch = 2
    customer = 3
    na = 4


partner_activity_code_mapping = {
    PartnerType.cif.value: ["branch_training", "branch_visit"],
    PartnerType.branch.value: ["hni_meet", "ice_activity"],
    PartnerType.customer.value: ["persistency_61", "persistency_13", "campaigns"],
    PartnerType.na.value: ['finish_day', 'expense_claim', 'refreshment_claim', 'none']
}


def get_lead_partner_type(activity_code):
    """
    This function simply returns the PartnerType for a given activity_code.
    :param activity_code:
    :return:
    """
    for k, v in partner_activity_code_mapping.items():
        if activity_code in v:
            return k
    return PartnerType.na.value
