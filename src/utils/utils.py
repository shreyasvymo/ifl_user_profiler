import numpy as np
import math
from pymongo import MongoClient


def data_normalization(data, norm_method="mm"):
    """
    This function normalizes the input set of numbers. This function also takes care of NaNs in the data.
    :param data: Here data is a list of numbers that needs to be normalized
    :param norm_method: Method that is used to normalise the data.
    :return:
    """
    import pandas as pd
    data = pd.Series(np.array(data, dtype='float64'))
    max_data = data.max()
    min_data = data.min()

    norm_data = data.sub(min_data).div((max_data - min_data))
    norm_data = norm_data.to_list()

    if norm_method == "mm":
        return norm_data
    elif norm_method == "mm_100":
        return [x*100 for x in norm_data]


def data_standardization(data, std_method="std"):
    std_data = []
    if std_method == "std":
        # Using Mean and Standard deviation - Standardization
        sigma = np.std(data)
        mean = np.mean(data)
        for data_point in data:
            data_point = (data_point - mean) / sigma
            std_data.append(data_point*100)
        return std_data


def nan_to_zero(abc):
    if isinstance(abc, str):
        abc = float(abc)
    if math.isnan(abc):
        return 0
    else:
        return abc


def query_db(db_name, collection_name, query_json={}, columns_dict=None, host_name="localhost", port=27017):

    client = MongoClient(host_name, port)
    db_obj = client.get_database(db_name)
    collection = db_obj.get_collection(collection_name)
    if columns_dict and len(columns_dict) > 0:
        cursor = collection.find(query_json, columns_dict)
    else:
        cursor = collection.find(query_json)

    client.close()
    return cursor


def get_time_transitions(updates_start_time):
    time_transitions = [0]
    updates_start_time = sorted(updates_start_time)
    if len(updates_start_time) == 0:
        return None
    prev_time = updates_start_time[0]
    for i in range(1, len(updates_start_time)):
        current_time = updates_start_time[i]
        time_transitions.append((current_time - prev_time)/3600)
        prev_time = current_time
    return time_transitions
