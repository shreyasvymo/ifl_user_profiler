from src.utils.lead_partner_activity_mapping import PartnerType, get_lead_partner_type


def get_activity_code(document):
    try:
        activity_code = document["inputs_map"]["_activity_code"]
    except KeyError:
        try:
            activity_code = document["updates_inputs_map"]["activity_new"]["name"]
        except KeyError:
            return None
    return activity_code


def lead_partner_identifier(lead_doc):
    """
    This function is used to identify the partner for each lead.
    :param lead_doc:
    :return:
    """
    partner_type = PartnerType.na.value

    if lead_doc["first_update_type"] == "activity_new":
        # Regular Leads' partner type is identified based on their activity_code.
        activity_code = get_activity_code(lead_doc)
        if activity_code:
            partner_type = get_lead_partner_type(activity_code)

    elif lead_doc["first_update_type"] == "persistency_new":
        # All the persistent leads are of type customer
        partner_type = PartnerType.customer.value

    else:
        pass
    return partner_type
