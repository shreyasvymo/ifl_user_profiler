from enum import Enum


class LeadStates(Enum):
    new_state = 1
    won_state = 2
    lost_state = 3
    met_state = 4
    unmet_open_state = 5
    rescheduled_state = 6
    unmet_lost_state = 7
    backlog_state = 8
    active_state = 9


lead_state_activity_mapping = {
    LeadStates.new_state.value: ['new', 'activity_new', 'branch_new', 'persistency_new', 'agency_persistency_new', 'business_new', 'agency_activity_new', 'new_recruitment_lead', 'rmgcl_activity_new', 'rmgcl_lead_new'],

    LeadStates.won_state.value: ['converted', 'activity_completed', 'persistency_converted', 'agency_persistency_converted', 'recruitment_converted', 'agency_activity_completed', 'business_converted', 'rmgcl_activity_completed', 'rmgcl_converted'],

    LeadStates.lost_state.value: ['dropped', 'activity_cancelled', 'persistency_dropped', 'agency_persistency_dropped', 'recruitment_dropped', 'agency_activity_cancelled', 'business_dropped', 'rmgcl_activity_cancelled', 'rmgcl_dropped'],

    LeadStates.met_state.value: ['met_scheduled',  'check_in', 'reschedule_meeting', 'converted',
                                 'persistency_followup', 'agency_persistency_followup', 'recruitment_converted',
                                 'rmgcl_met_scheduled', 'rmgcl_followup', 'rmgcl_converted'],

    LeadStates.unmet_open_state.value: ['new', 'callback', 'met_scheduled', 'reschedule_meeting', 'persistency_callback', 'agency_persistency_callback', 'persistency_met_scheduled', 'agency_persistency_met_scheduled', 'business_call_back', 'business_meeting_scheduled', 'business_meeting_reschedule', 'business_follow_up_meeting', 'rmgcl_callback'],

    LeadStates.rescheduled_state.value: ['resched', 'met_scheduled', 'reschedule_meeting', 'activity_rescheduled', 'persistency_reschedule_meeting', 'agency_persistency_reschedule_meeting', 'business_call_back', 'business_meeting_scheduled', 'business_meeting_reschedule', 'business_follow_up_meeting', 'rmgcl_reschedule_meeting'],

    LeadStates.unmet_lost_state.value: []

}