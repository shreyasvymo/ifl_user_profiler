import numpy as np
from src.utils.utils import data_normalization
from src.partner_lead_computer.cif_lead_computer import compute_lead_cif_score
from src.partner_lead_computer.branch_lead_computer import compute_lead_branch_score
from src.partner_lead_computer.customer_lead_computer import compute_lead_customer_score
from src.utils.lead_partner_identifier import PartnerType
from src.utils.utils import nan_to_zero
import pandas as pd


def get_conv_ratio(actual_lead_list, expected_lead_list):
    # return [(x-y)/x if x != 0 else y for x, y in zip(actual_lead_list, expected_lead_list)]
    return [(x-y)/x if x != 0 else 0 for x, y in zip(actual_lead_list, expected_lead_list)]


def set_lead_scores(leads_data_frame):
    # Assign the lead scores based on the partner type
    for ind, row in leads_data_frame.iterrows():
        if PartnerType[row["partner_type"]].value == PartnerType.cif.value:
            lead_cif_score = compute_lead_cif_score(row)
            if lead_cif_score:
                leads_data_frame.loc[ind, "lead_eng_score"] = lead_cif_score[0]
                leads_data_frame.loc[ind, "lead_sourcing_score"] = lead_cif_score[1]
                leads_data_frame.loc[ind, "lead_business_score"] = lead_cif_score[2]
                leads_data_frame.loc[ind, "lead_fp_score"] = lead_cif_score[3]
                leads_data_frame.loc[ind, "lead_score"] = sum(lead_cif_score)
                leads_data_frame.loc[ind, "lead_effort_score"] = sum(lead_cif_score[:2]) + lead_cif_score[-1]
                leads_data_frame.loc[ind, "lead_outcome_score"] = lead_cif_score[2]

        elif PartnerType[row["partner_type"]].value == PartnerType.customer.value:
            lead_customer_score = compute_lead_customer_score(row)
            if lead_customer_score:
                leads_data_frame.loc[ind, "lead_eng_score"] = lead_customer_score[0]
                leads_data_frame.loc[ind, "lead_sourcing_score"] = lead_customer_score[1]
                leads_data_frame.loc[ind, "lead_business_score"] = lead_customer_score[2]
                leads_data_frame.loc[ind, "lead_fp_score"] = lead_customer_score[3]
                leads_data_frame.loc[ind, "lead_score"] = sum(lead_customer_score)
                leads_data_frame.loc[ind, "lead_effort_score"] = sum(lead_customer_score[:2]) + lead_customer_score[-1]
                leads_data_frame.loc[ind, "lead_outcome_score"] = lead_customer_score[2]

        elif PartnerType[row["partner_type"]].value == PartnerType.branch.value:
            lead_branch_score = compute_lead_branch_score(row)
            if lead_branch_score:
                leads_data_frame.loc[ind, "lead_eng_score"] = lead_branch_score[0]
                leads_data_frame.loc[ind, "lead_sourcing_score"] = lead_branch_score[1]
                leads_data_frame.loc[ind, "lead_business_score"] = lead_branch_score[2]
                leads_data_frame.loc[ind, "lead_fp_score"] = lead_branch_score[3]
                leads_data_frame.loc[ind, "lead_score"] = sum(lead_branch_score)
                leads_data_frame.loc[ind, "lead_effort_score"] = sum(lead_branch_score[:2]) + lead_branch_score[-1]
                leads_data_frame.loc[ind, "lead_outcome_score"] = lead_branch_score[2]

    return leads_data_frame


def normalise_data_frame(data_frame, norm_col_list):
    for col in norm_col_list:
        data_frame[col] = data_normalization(data_frame[col], "mm_100")
    return data_frame


def set_group_wise_attributes(group_data_frame, leads_data_frame):
    # Compute and set num activities variance
    median_num_updates = np.median([len(eval(i)) for i in group_data_frame["update_types"]])

    # Update all the Num Act Variance attributes
    updates_tat = []
    for ind, row in group_data_frame.iterrows():
        db_id = row["db_id"]
        leads_data_frame.loc[leads_data_frame["db_id"] == db_id, "num_act_var"] = median_num_updates - len(eval(row["update_types"]))
        # Get all TATs
        all_updates_timestamps = sorted(eval(row["update_timestamps"]))
        updates_tat.append(all_updates_timestamps[-1] - all_updates_timestamps[0])

    # Update tat by comparing with the peers in lead data-frame.
    mean_tat = np.mean(updates_tat)
    for ind, row in group_data_frame.iterrows():
        db_id = row["db_id"]
        all_updates_timestamps = sorted(eval(row["update_timestamps"]))
        tat = all_updates_timestamps[-1] - all_updates_timestamps[0]
        leads_data_frame.loc[leads_data_frame["db_id"] == db_id, "tat_var"] = mean_tat - tat

    return leads_data_frame


def set_derived_data_group_wise(leads_data_frame):
    # Compute varied number of activities and TAT group wise.
    groups = leads_data_frame.groupby(['partner_type', 'lead_state'])
    for group in groups:
        leads_data_frame = set_group_wise_attributes(group[1], leads_data_frame)

    leads_data_frame["conv_ratio"] = get_conv_ratio([nan_to_zero(x) for x in leads_data_frame["number_leads"].to_list(
    )], [nan_to_zero(x) for x in leads_data_frame["expected_leads"].to_list()])

    return leads_data_frame


if __name__ == '__main__':
    import time
    st = time.time()
    num_cifs_dict = {}
    leads = pd.read_csv("/Users/shreyasrk/PycharmProjects/ifl_user_profiler/data/leads/leads_df.tsv", sep="\t")
    leads = set_derived_data_group_wise(leads)
    list_of_cols = ["dist_travelled", "ticket_size", "expected_premium", "premium", "branch_size",
                    "meeting_duration", "conv_ratio"]
    leads = normalise_data_frame(leads, list_of_cols)
    leads = set_lead_scores(leads)
    leads.to_csv("/Users/shreyasrk/PycharmProjects/ifl_user_profiler/data/leads/leads_normalised.tsv", sep="\t",
                 index=False)
    et = time.time()
    print(et-st)
