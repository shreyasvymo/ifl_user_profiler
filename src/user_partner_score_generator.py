from enum import Enum
from src.partner_lead_computer.cif_lead_computer import compute_lead_cif_score
from src.partner_lead_computer.customer_lead_computer import compute_lead_customer_score
from src.partner_lead_computer.branch_lead_computer import compute_lead_branch_score
from src.utils.lead_partner_identifier import PartnerType
from src.post_process_leads import normalise_data_frame
import numpy as np
import pandas as pd
from collections import Counter


class UserCifAttributes(Enum):
    """
    Set of attributes used to compute the User score for CIFs
    """
    tenure = .18
    avg_visits_per_week = .12
    avg_cif_lead_score = .50
    user_conversion_rate = .20


class UserBranchAttributes(Enum):
    """
    Set of attributes used to compute the User score for CIFs
    """
    tenure = .18
    avg_visits_per_week = .12
    avg_branch_lead_score = .50
    user_conversion_rate = .20


class UserCustomerAttributes(Enum):
    """
    Set of attributes used to compute the User score for CIFs
    """
    tenure = .18
    avg_visits_per_week = .12
    avg_customer_lead_score = .50
    user_conversion_rate = .20


def compute_user_cif_score(data_frame):
    # Input data_frame is grouped by (user_id, partner_type)
    # In this case the partner_type is "CIF"
    total_leads = len(data_frame)
    try:
        converted_leads = dict(Counter(data_frame["lead_state"].to_list()))["won_state"]
    except KeyError:
        converted_leads = 0
    all_leads_scores = []
    for ind, row in data_frame.iterrows():
        all_scores = compute_lead_cif_score(row)
        lead_score = sum(all_scores)
        if lead_score:
            all_leads_scores.append(lead_score)
    if len(all_leads_scores) > 0:
        avg_lead_score = np.mean(all_leads_scores)
    else:
        avg_lead_score = 0
    first_lead_timestamp = data_frame["server_date"].min()
    last_lead_timestamp = data_frame["server_date"].max()
    tenure = last_lead_timestamp - first_lead_timestamp
    tenure_in_weeks = max(1, tenure/(60*60*24*7))
    visits_per_week = len(data_frame)/tenure_in_weeks

    return UserCifAttributes.tenure.value * tenure_in_weeks + UserCifAttributes.avg_visits_per_week.value * \
           visits_per_week + UserCifAttributes.avg_cif_lead_score.value * avg_lead_score + \
           UserCifAttributes.user_conversion_rate.value * (converted_leads * 100/total_leads)


def compute_user_branch_score(data_frame):
    # Input data_frame is grouped by (user_id, partner_type)
    # In this case the partner_type is "branch"
    total_leads = len(data_frame)
    try:
        converted_leads = dict(Counter(data_frame["lead_state"].to_list()))["won_state"]
    except KeyError:
        converted_leads = 0
    all_leads_scores = []
    for ind, row in data_frame.iterrows():
        lead_score = compute_lead_branch_score(row)
        lead_score = sum(lead_score)
        if lead_score:
            all_leads_scores.append(lead_score)
    if len(all_leads_scores) > 0:
        avg_lead_score = np.mean(all_leads_scores)
    else:
        avg_lead_score = 0

    first_lead_timestamp = data_frame["server_date"].min()
    last_lead_timestamp = data_frame["server_date"].max()
    tenure = last_lead_timestamp - first_lead_timestamp
    tenure_in_weeks = max(1, tenure / (60 * 60 * 24 * 7))
    visits_per_week = len(data_frame) / tenure_in_weeks

    return UserBranchAttributes.tenure.value * tenure_in_weeks + UserBranchAttributes.avg_visits_per_week.value * \
           visits_per_week + UserBranchAttributes.avg_branch_lead_score.value * avg_lead_score + \
           UserBranchAttributes.user_conversion_rate.value * (converted_leads*100/total_leads)


def compute_user_customer_score(data_frame):
    # Input data_frame is grouped by (user_id, partner_type)
    # In this case the partner_type is "customer"
    total_leads = len(data_frame)
    try:
        converted_leads = dict(Counter(data_frame["lead_state"].to_list()))["won_state"]
    except KeyError:
        converted_leads = 0
    all_leads_scores = []
    for ind, row in data_frame.iterrows():
        lead_score = compute_lead_customer_score(row)
        lead_score = sum(lead_score)
        if lead_score:
            all_leads_scores.append(lead_score)

    if len(all_leads_scores) > 0:
        avg_lead_score = np.mean(all_leads_scores)
    else:
        avg_lead_score = 0

    first_lead_timestamp = data_frame["server_date"].min()
    last_lead_timestamp = data_frame["server_date"].max()
    tenure = last_lead_timestamp - first_lead_timestamp
    tenure_in_weeks = max(1, tenure / (60 * 60 * 24 * 7))
    visits_per_week = len(data_frame) / tenure_in_weeks

    return UserCustomerAttributes.tenure.value * tenure_in_weeks + UserCustomerAttributes.avg_visits_per_week.value * \
           visits_per_week + UserCustomerAttributes.avg_customer_lead_score.value * avg_lead_score + \
           UserCustomerAttributes.user_conversion_rate.value * (converted_leads*100/total_leads)


def populate_user_df(data_frame):
    users = pd.DataFrame(columns=["user_id", "cif_score", "branch_score", "customer_score"])
    # group data by (user, partner_type)
    groups = data_frame.copy().groupby(["user_id", "partner_type"])

    for group in groups:
        user_dict = {}
        user_id = group[0][0]
        user_dict["user_id"] = user_id
        partner_type = group[0][1]

        # Normalise certain fields group wise:
        norm_data_frame = group[1].copy()

        if PartnerType[partner_type].value == PartnerType.cif.value:
            user_dict["cif_score"] = compute_user_cif_score(norm_data_frame)
        elif PartnerType[partner_type].value == PartnerType.customer.value:
            user_dict["customer_score"] = compute_user_customer_score(norm_data_frame)
        elif PartnerType[partner_type].value == PartnerType.branch.value:
            user_dict["branch_score"] = compute_user_branch_score(norm_data_frame)
        else:
            pass

        # To ensure only one user entry is there in the table.
        if len(users) == 0 or user_dict["user_id"] not in users["user_id"].to_list():
            users = users.append(user_dict, ignore_index=True)
        elif len(user_dict) > 1:
            for k, v in user_dict.items():
                if k != "user_id":
                    users.loc[users["user_id"] == user_id, k] = v

    return users


def dump_user_scores_by_partner(leads_df):
    groups = leads_df.groupby(["user_id", "partner_type"]).agg(
        {"lead_eng_score": "mean", "lead_sourcing_score": "mean", "lead_business_score": "mean", "lead_fp_score":
            "mean", "user_id": "first", "partner_type": "first"})
    groups.dropna(inplace=True)
    groups.to_csv("/Users/shreyasrk/PycharmProjects/ifl_user_profiler/result/user_partner.csv", index=False)
    return groups


if __name__ == '__main__':
    import time
    st = time.time()
    leads = pd.read_csv("/Users/shreyasrk/PycharmProjects/ifl_user_profiler/data/leads/leads_normalised.tsv", sep="\t")

    users_df = populate_user_df(leads)
    # Dump user table after computation
    users_df.to_csv("/Users/shreyasrk/PycharmProjects/ifl_user_profiler/result/user_score.csv", index=False)
    et = time.time()
    print(et-st)
