import pandas as pd
from src.utils.utils import query_db


def create_branches_df(branches_documents, cif_count_dict):
    branches_data_frame = pd.DataFrame(columns=["branch_code", "branch_name", "branch_lead_code", "branch_size"])
    for doc in branches_documents:
        new_branch = {}
        try:
            new_branch["branch_lead_code"] = doc["code"]
        except KeyError:
            pass
        try:
            new_branch["branch_code"] = doc["inputs_map"]["branch_code"]
        except KeyError:
            pass
        try:
            new_branch["branch_name"] = doc["inputs_map"]["name"]
        except KeyError:
            pass
        # Get number of CIFS
        try:
            new_branch["branch_size"] = cif_count_dict[doc["inputs_map"]["branch_code"]]
        except KeyError:
            pass
        branches_data_frame = branches_data_frame.append(new_branch, ignore_index=True)

    return branches_data_frame


def create_lgsp_count_dict(lgsp_leads):
    branch_count = {}
    for row in lgsp_leads:
        try:
            branch_count[row['inputs_map']['branch_code']] += 1
        except KeyError:
            branch_count[row['inputs_map']['branch_code']] = 1
    return branch_count


if __name__ == '__main__':
    import time
    st = time.time()
    db = "vymo-lms"
    table = "indiafirstlife_branches"
    lgsp_table_name = "indiafirstlife_lgsp_leads"
    all_branches = query_db(db, table)
    all_lgsp = query_db(db, lgsp_table_name)

    lgsp_dict = create_lgsp_count_dict(all_lgsp)
    print(lgsp_dict)
    branches_df = create_branches_df(all_branches, lgsp_dict)
    branches_df.to_csv("/Users/shreyasrk/PycharmProjects/ifl_user_profiler/data/branch/branches.csv", index=False)
    print("Time taken", time.time() - st)
