import pandas as pd
from sklearn.cluster import KMeans
from src.utils.utils import data_normalization
import plotly.graph_objs as go
from plotly.offline import plot


def label_point(x, y, val, ax):
    a = pd.concat({'x': x, 'y': y, 'val': val}, axis=1)
    for i, point in a.iterrows():
        ax.text(point['x'], point['y'], str(point['val']))


def create_clusters(data_frame):
    copy_df = data_frame.copy()
    data_frame.drop(["user_id"], axis=1, inplace=True)

    for col in data_frame.columns:
        data_frame[col] = data_normalization(data_frame[col], norm_method="mm_100")

    kmeans = KMeans(n_clusters=3)
    kmeans.fit(data_frame)
    labels = kmeans.predict(data_frame)

    copy_df["cluster"] = labels
    return copy_df


def create_trace(x, y, user_ids, color):
    trace = go.Scatter(
        x=x,
        y=y,
        text=user_ids,
        mode='markers',
        marker=dict(color=color)
    )
    cluster = [dict(type='ellipse',
                    xref='x', yref='y',
                    x0=min(x),
                    y0=min(y),
                    x1=max(x),
                    y1=max(y),
                    opacity=.25,
                    line=dict(color=color),
                    fillcolor=color)]
    return trace, cluster


def group_by_cluster_create_plot(data_frame, col_name, plot_title='User Tier Clustering'):
    groups = data_frame.groupby(['cluster'])
    data = []
    clusters = []
    colors = ["#1452b7", "#a33111", "#18db86"]
    for i, group in enumerate(groups):
        df = group[1]
        trace, cluster = create_trace(list(df.index.values), df[col_name].to_list(), df["user_id"].to_list(),
                                      colors[i])
        data.append(trace)
        clusters.append(cluster)

    updatemenus = list([
        dict(type="buttons",
             buttons=list([
                 dict(label='None',
                      method='relayout',
                      args=['shapes', []]),
                 dict(label='Cluster 0',
                      method='relayout',
                      args=['shapes', clusters[0]]),
                 dict(label='Cluster 1',
                      method='relayout',
                      args=['shapes', clusters[1]]),
                 dict(label='Cluster 2',
                      method='relayout',
                      args=['shapes', clusters[2]]),
                 dict(label='All',
                      method='relayout',
                      args=['shapes', clusters[0] + clusters[1] + clusters[2]])
             ]),
             )
    ])

    layout = dict(title=plot_title, showlegend=True, updatemenus=updatemenus)

    return dict(data=data, layout=layout)


if __name__ == '__main__':
    # Form a standard dataframe with all the features.
    # Normalise each column in the dataframe
    # Run and predict kmeans cluster
    user_scores = pd.read_csv("/Users/shreyasrk/PycharmProjects/ifl_user_profiler/result/user_df.csv")

    all_params_df = user_scores[["user_id", "cif_score"]].copy()
    all_params_df.fillna(-1, inplace=True)

    cluster_df = create_clusters(all_params_df)

    fig = group_by_cluster_create_plot(cluster_df)

    plot(fig, filename='../result/user_clustering.html')
