from enum import Enum
from src.partner_lead_computer.cif_lead_computer import lead_funnel_progression_score
from src.utils.utils import nan_to_zero


class BranchLeadAttributes(Enum):
    """
    This lists down attributes used to compute the lead level score for every Branch lead.
    """
    # Lead Engagement attributes.
    # TODO: Engagement Type score needs to be addressed once email, call data is integrated.
    engagement_type = .10 # meeting, call or email?
    engagement_duration = .05
    planning_score = .10
    coverage_per = .10

    # Lead Sourcing attributes.
    num_cifs = .10
    dist_travelled = .05
    num_activities = .10

    # Funnel Progression attributes.
    tat = .10
    updates_transition = .10

    # Business generation attributes.
    ticket_size = .10
    lead_conversion = .10


def compute_lead_branch_score(doc):

    # FIXME: engagement_type, coverage_per should be set when necessary data is available.
    engagement_type = 100
    coverage_per = 100
    # Compute lead score for every CIF lead given.
    meeting_duration = nan_to_zero(doc["meeting_duration"])
    ticket_size = nan_to_zero(doc["ticket_size"])
    num_act_var = nan_to_zero(doc["num_act_var"])
    updates_transition_score, pre_plan_score = lead_funnel_progression_score(doc["update_types"],
                                                                          eval(doc["update_planned_timestamps"]),
                                                                          eval(doc["update_timestamps"]))
    num_cifs = nan_to_zero(doc["branch_size"])
    tat = nan_to_zero(doc["tat_var"])
    dist_travelled = nan_to_zero(doc["dist_travelled"])

    lead_conversion_score = 0.7 * nan_to_zero(doc["conv_ratio"]) + 0.3 * nan_to_zero(doc["expected_premium"])

    lead_eng_score = engagement_type * BranchLeadAttributes.engagement_type.value + meeting_duration *\
                     BranchLeadAttributes.engagement_duration.value + pre_plan_score *\
                     BranchLeadAttributes.planning_score.value + coverage_per * BranchLeadAttributes.coverage_per.value

    lead_sourcing_score = num_act_var * BranchLeadAttributes.num_activities.value + num_cifs *\
                          BranchLeadAttributes.num_cifs.value + dist_travelled * BranchLeadAttributes.dist_travelled.\
                          value

    lead_business_score = lead_conversion_score * BranchLeadAttributes.lead_conversion.value + ticket_size * \
                          BranchLeadAttributes.ticket_size.value

    lead_fp_score = updates_transition_score * BranchLeadAttributes.updates_transition.value + tat * \
                    BranchLeadAttributes.tat.value

    return lead_eng_score, lead_sourcing_score, lead_business_score, lead_fp_score
