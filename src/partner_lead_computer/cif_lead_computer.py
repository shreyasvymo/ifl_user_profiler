from enum import Enum
from src.utils.lead_state_identifier import LeadStates, get_update_type
import numpy as np
from src.utils.utils import nan_to_zero, get_time_transitions


class CifLeadAttributes(Enum):
    """
    This lists down attributes used to compute the lead level score for every CIF lead.
    """
    # Lead Engagement attributes.
    # TODO: Engagement Type score needs to be addressed when email, call data is integrated.
    engagement_type = .10  # meeting, call or email?
    engagement_duration = .05
    planning_score = .10
    coverage_per = .10

    # Lead Sourcing attributes.
    self_assigned = .05
    num_cifs = .05
    dist_travelled = .05
    num_activities = .10

    # Funnel Progression attributes.
    tat = .10
    updates_transition = .10

    # Business generation attributes.
    ticket_size = .10
    lead_conversion = .10


class LeadUpdateTimeThreshold(Enum):
    """
    This is a list of Optimal transition times in hours for Lead's Updates.
    """
    meeting = 24
    won = 24
    lost = 12
    pass


def lead_funnel_progression_score(list_of_updates, list_of_updates_timestamps, list_of_planned_timestamps):
    """
    In this function we compute the score for the updates by considering the updates' transitions, TATs and pre-plans
    """
    lead_fp_score = 10
    dwell_time_in_hours = get_time_transitions(list_of_updates_timestamps)

    for i, update in enumerate(list_of_updates):
        update_type = get_update_type(update)
        if update_type == LeadStates.won_state.value:
            if dwell_time_in_hours[i] <= LeadUpdateTimeThreshold.won.value:
                lead_fp_score += 7
            else:
                lead_fp_score += 3
        elif update_type == LeadStates.lost_state.value:
            if dwell_time_in_hours[i] <= LeadUpdateTimeThreshold.lost.value:
                lead_fp_score -= 3
            else:
                lead_fp_score -= 7
        elif update_type == LeadStates.met_state.value:
            if dwell_time_in_hours[i] > LeadUpdateTimeThreshold.meeting.value:
                lead_fp_score -= 2
            else:
                pass
        elif update_type == LeadStates.rescheduled_state.value:
            lead_fp_score -= 2

    # Consider pre-planing of updates within a lead. Number of days of preplanning, clipped to 0
    updates_pre_planning_score = max(0, sum(np.subtract(list_of_updates_timestamps, list_of_planned_timestamps)) / (
            60 * 60 * 24))
    return lead_fp_score, updates_pre_planning_score


def compute_lead_cif_score(doc):

    # FIXME: engagement_type, coverage_per should be set when necessary data is available.
    engagement_type = 100
    coverage_per = 100

    # Compute lead score for every CIF lead given.
    meeting_duration = nan_to_zero(doc["meeting_duration"])
    ticket_size = nan_to_zero(doc["ticket_size"])
    num_act_var = nan_to_zero(doc["num_act_var"])
    self_sourced = doc["self_sourced"]
    fp_score, pre_plan_score = lead_funnel_progression_score(eval(doc["update_types"]),
                                                             eval(doc["update_planned_timestamps"]),
                                                             eval(doc["update_timestamps"]))
    num_cifs = nan_to_zero(doc["branch_size"])
    tat = nan_to_zero(doc["tat_var"])
    dist_travelled = nan_to_zero(doc["dist_travelled"])

    lead_conversion_score = 0.7 * nan_to_zero(doc["conv_ratio"]) + 0.3 * nan_to_zero(doc["expected_premium"])

    lead_eng_score = engagement_type * CifLeadAttributes.engagement_type.value +\
                     meeting_duration * CifLeadAttributes.engagement_duration.value +\
                     pre_plan_score * CifLeadAttributes.planning_score.value +\
                     coverage_per * CifLeadAttributes.coverage_per.value

    lead_sourcing_score = num_act_var * CifLeadAttributes.num_activities.value + \
                          num_cifs * CifLeadAttributes.num_cifs.value + \
                          dist_travelled * CifLeadAttributes.dist_travelled.value +\
                          self_sourced * CifLeadAttributes.self_assigned.value

    lead_business_score = lead_conversion_score * CifLeadAttributes.lead_conversion.value +\
                          ticket_size * CifLeadAttributes.ticket_size.value

    lead_fp_score = fp_score * CifLeadAttributes.updates_transition.value + \
                    tat * CifLeadAttributes.tat.value

    return lead_eng_score, lead_sourcing_score, lead_business_score, lead_fp_score
