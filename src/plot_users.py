import plotly.graph_objs as go
from plotly.offline import plot
import pandas as pd
from plotly import figure_factory as ff
from src.utils.utils import data_standardization


def create_dist_plot(user_scores, file_path):
    fig = ff.create_distplot([user_scores], ["User scores"], bin_size=33.33)
    plot(fig, filename=file_path)
    return None


def plot_users_partner_type(user_ids, scores, name, variance):
    x_col = list(range(1, len(user_ids) + 1))
    trace_b = go.Bar(
        x=x_col,
        y=scores,
        name=name,
        text=user_ids,
        error_y=dict(
            type='data',
            array=variance,
            arrayminus=[],
            visible=True
        )
    )
    return trace_b


def all_plots(all_user_data):
    trace_names = {"cif_score": ["CIFs"], "branch_score": ["Bank Branch"], "customer_score": ["Direct Customer"]}
    traces = []
    user_ids = all_user_data["user_id"].to_list()

    # Adding the scores' mean value
    for k, v in trace_names.items():
        trace_names[k].append(all_user_data[k].mean())

    for col in all_user_data.columns:
        if col != "user_id":
            # Compute variance array
            variance_array = [trace_names[col][1]-i for i in all_user_data[col].to_list()]
            traces.append(plot_users_partner_type(user_ids, all_user_data[col].to_list(), trace_names[col][0],
                                                  variance_array))

    layout = go.Layout(showlegend=True,
                       title="User Ranks",
                       xaxis=dict(title="User IDs"),
                       yaxis=dict(title="User Score"),
                       hovermode='closest',
                       barmode='relative')

    fig = go.Figure(data=traces, layout=layout)
    plot(fig, filename='/Users/shreyasrk/PycharmProjects/ifl_user_profiler/result/all_user_gen_plot.html')


def plot_user_effort_outcome_quadrant(leads_df):
    """
    This function plots the effort v/s outcome graph.
    :param leads_df:
    :return:
    """
    user_effort_df = pd.DataFrame(columns=["user_id", "effort", "outcome"])

    groups = leads_df.copy().groupby(["user_id"]).agg({"lead_effort_score": "mean", "lead_outcome_score": "mean"})
    for group in groups.iterrows():
        user_effort_df = user_effort_df.append({"user_id": group[0], "effort": group[1]["lead_effort_score"],
                                                "outcome": group[1]["lead_outcome_score"]}, ignore_index=True)

    user_effort_df.dropna(inplace=True)
    all_users = user_effort_df["user_id"].to_list()
    all_efforts = data_standardization(user_effort_df["effort"].to_list())
    all_outcomes = data_standardization(user_effort_df["outcome"].to_list())

    user_effort_df.to_csv("/Users/shreyasrk/PycharmProjects/ifl_user_profiler/result/user_effort.csv", index=False)
    # Plot Efforts v/s Outcome graph.
    trace = go.Scatter(x=all_efforts, y=all_outcomes, mode="markers", text=all_users)
    data = [trace]

    layout = go.Layout(
        title=go.layout.Title(
            text='Efforts v/s Outcome'
        ),
        xaxis=go.layout.XAxis(
            title=go.layout.xaxis.Title(
                text='Efforts'
            )
        ),
        yaxis=go.layout.YAxis(
            title=go.layout.yaxis.Title(
                text='Outcomes'
            )
        )
    )
    plot(go.Figure(data, layout), filename="/Users/shreyasrk/PycharmProjects/ifl_user_profiler/result/"
                                           "outcome_vs_effort_quadrant.html")


if __name__ == '__main__':
    # user_data = pd.read_csv("/Users/shreyasrk/PycharmProjects/ifl_user_profiler/result/user_score.csv")
    # user_data.fillna(-1, inplace=True)
    # all_plots(user_data)

    all_leads = pd.read_csv("/Users/shreyasrk/PycharmProjects/ifl_user_profiler/result/leads_normalised.tsv", sep="\t")
    plot_user_effort_outcome_quadrant(all_leads)
    pass
