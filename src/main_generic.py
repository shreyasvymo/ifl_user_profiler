from src.create_lead_df import process_mongo_leads
from src.utils.utils import query_db
from src.post_process_leads import normalise_data_frame, set_derived_data_group_wise, set_lead_scores
from src.user_partner_score_generator import populate_user_df, dump_user_scores_by_partner
from src.create_branch_df import create_branches_df, create_lgsp_count_dict
from src.plot_users import all_plots, create_dist_plot, plot_user_effort_outcome_quadrant
import pandas as pd
from src.kmeans import create_clusters, group_by_cluster_create_plot
from plotly.offline import plot
import os
import json


def main():
    mode = "local"
    root_path = os.path.realpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

    if mode == "local":
        """
        Add code to create data & result directories.
        """
        os.makedirs(os.path.join(root_path, "data", "branch"), exist_ok=True)
        os.makedirs(os.path.join(root_path, "data", "leads"), exist_ok=True)
        os.makedirs(os.path.join(root_path, "result"), exist_ok=True)

        # Get LMS data form Mongo DB and store it in a DataFrame
        db = "vymo-lms"
        ifl_leads_collection_name = "indiafirstlife_leads"
        ifl_leads_per_collection_name = "indiafirstlife_per_leads"
        ifl_lgsp_collection_name = "indiafirstlife_lgsp_leads"
        ifl_branches_collection_name = "indiafirstlife_branches"

        leads = pd.DataFrame()

        all_lgsp = query_db(db, ifl_lgsp_collection_name)
        lgsp_dict = create_lgsp_count_dict(all_lgsp)

        # Create branches dataframe
        branches = create_branches_df(query_db(db, ifl_branches_collection_name), lgsp_dict)
        branches.to_csv(os.path.join(root_path, "data/branch/branches.csv"), index=False)
        print("Branches Created. Time taken", st-time.time())

        # Process LMS data
        leads = process_mongo_leads(query_db(db, ifl_leads_collection_name), leads, branches)
        leads = process_mongo_leads(query_db(db, ifl_leads_per_collection_name), leads, branches)
        leads.to_csv(os.path.join(root_path, "data/leads/leads_df.tsv"), sep="\t", index=False)
        print("Leads Created. Time taken", st-time.time())

        # Here derived data fields like tat_var, num_act_var and branch size are added.
    else:
        # Things to do in remote mode

        branch_count = {}
        path_to_lgsp = ""
        path_to_branch = ""
        path_to_leads = ""
        path_to_per_leads = ""

        # Create LGSP dictionary
        with open(path_to_lgsp) as lgsp_mapping:
            for line in lgsp_mapping:
                row = json.loads(line)
                try:
                    branch_count[row['inputs_map']['branch_code']] += 1
                except KeyError:
                    branch_count[row['inputs_map']['branch_code']] = 1

        # Create branches df
        branches = pd.DataFrame()
        with open(path_to_branch) as branches_f:
            for line in branches_f:
                row = json.loads(line)
                branches = branches.append(create_branches_df([row], branch_count), ignore_index=True)

        # Create leads df
        leads = pd.DataFrame()
        with open(path_to_leads) as leads_f:
            for line in leads_f:
                row = json.loads(line)
                leads = leads.append(process_mongo_leads([row], leads, branches), ignore_index=True)

        # Append persistency lead data to Leads table
        with open(path_to_per_leads) as per_leads_f:
            for line in per_leads_f:
                row = json.loads(line)
                leads = leads.append(process_mongo_leads([row], leads, branches), ignore_index=True)

    leads = set_derived_data_group_wise(leads)
    list_of_cols = ["dist_travelled", "ticket_size", "expected_premium", "premium", "branch_size",
                    "meeting_duration", "conv_ratio", "tat_var", "num_act_var"]
    leads = normalise_data_frame(leads, list_of_cols)
    leads = set_lead_scores(leads)
    leads.to_csv(os.path.join(root_path, "data/leads/leads_normalised.tsv"), sep="\t", index=False)

    print("Added derived data into leads and normalised the below fields")
    print(list_of_cols)
    print("Time taken", st - time.time())

    # Add logic to create user dumps for partners with profile scores.
    dump_user_scores_by_partner(leads)

    # Create User-wise effort vs Outcome graph
    plot_user_effort_outcome_quadrant(leads)

    # Create User Profile scores
    user_df = populate_user_df(leads)

    # Plot the user data-frame
    user_df.fillna(-1, inplace=True)
    user_df.to_csv(os.path.join(root_path, "result/user_df.csv"), index=False)
    all_plots(user_df)

    # Cluster the user based on the scores generated
    all_cif = user_df[["user_id", "cif_score"]].copy()
    all_branch = user_df[["user_id", "branch_score"]].copy()
    all_customer = user_df[["user_id", "customer_score"]].copy()

    # Overall score
    user_df["total_score"] = user_df["cif_score"] + user_df["branch_score"] + user_df["customer_score"]
    all_users_overall = user_df[["user_id", "total_score"]].copy()

    clustered_cif = create_clusters(all_cif)
    plot(group_by_cluster_create_plot(clustered_cif, "cif_score", plot_title="CIF based User Tiers"),
         filename=os.path.join(root_path, 'result/user_cif_clustering.html'))

    clustered_branch = create_clusters(all_branch)
    plot(group_by_cluster_create_plot(clustered_branch, "branch_score", plot_title="Branch based User Clusters"),
         filename=os.path.join(root_path, 'result/user_branch_clustering.html'))

    clustered_customer = create_clusters(all_customer)
    plot(group_by_cluster_create_plot(clustered_customer, "customer_score", plot_title="Customer based User Clusters"),
         filename=os.path.join(root_path, 'result/user_customer_clustering.html'))

    clustered_users = create_clusters(all_users_overall)
    plot(group_by_cluster_create_plot(clustered_users, "total_score", plot_title="User Tiers"),
         filename=os.path.join(root_path, 'result/user_overall_tiers.html'))

    # Plot distribution plot of the user score
    create_dist_plot(clustered_users["total_score"].to_list(), file_path=os.path.join(root_path,
                                                                         'result/user_overall_distribution.html'))


if __name__ == '__main__':
    import time
    st = time.time()
    main()
    et = time.time()
    print(et-st)
